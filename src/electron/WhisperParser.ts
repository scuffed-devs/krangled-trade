const { v4 } = require( 'uuid' );
const stashRegex = '\\".*\\"[,;].*(\\d{1,2}),?.*(\\d{1,2})';
const stashTest = new RegExp(stashRegex);



export class TradeParser {

  constructor() {
  }

  isTradeWhisper = (whisper: string) =>  {

    return stashTest.test(whisper);
  }

  parse = (whisper: string ) => {
    if (!this.isTradeWhisper(whisper))
    {
      return {};
    }

    const match = stashTest[Symbol.match](whisper);

    return {
      uuid: v4(),
      stashPosX: match ? match : [1],
      stashPosY: match ? match : [2]
    };
  }
}
