import {Stats} from 'fs';

const {app, Menu, Tray, nativeImage, screen, ipcMain} = require('electron');
const {resolve} = require('path');
const fs = require('fs');

import {TradeParser} from './WhisperParser';
import {Settings} from './electron-components/Settings';
import {WindowManager} from "./electron-components/WindowManager";

const windowManager: WindowManager = new WindowManager();

let display: Electron.Display;
let dimensions: Electron.Size;

let tray: Electron.Tray;
let canQuit = false;




let fileName: string = 'settings';
let settings: Settings = Settings.readSettings(fileName);


function createWindowFocus(){

  let mainWindow = windowManager.createWindow( {
    width: 400,
    height: 100,
    x: settings.posX,
    y: settings.posY,
    focusable: true,
    icon: './src/favicon.ico',
    frame: false,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  mainWindow.setAlwaysOnTop(true, "screen-saver")     // - 2 -
  mainWindow.setVisibleOnAllWorkspaces(true)          // - 3 -
  const ind = resolve('./dist/krangled-trade/index.html');
  mainWindow.loadURL(ind).then(() => console.log(`Loaded: ${ind}`));

  windowManager.mainWindow = mainWindow;
}

app.whenReady().then(() => {
  // grab screen dims, once ready
  display = screen.getPrimaryDisplay();
  dimensions = display.workAreaSize;
  createWindowFocus();
  initTray();
});


function initTray(): void {
  try {
    const icon: string = resolve('./src/assets/images/tray_icon.jpg');
    const img: Electron.NativeImage = nativeImage.createFromPath(icon);
    console.log(icon);
    tray = new Tray(img);
  } catch (e) {
    console.log(e);
  }
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Exit', click: _ => {
        canQuit = true;
        app.quit();
      }
    }
  ]);
  tray.setToolTip('krangled-trade at your service.');
  tray.setContextMenu(contextMenu);
}

app.on('before-quit', event => {
  if (!canQuit) {
    event.preventDefault();
  }
});

app.on('activate', () => {
  if (null === windowManager.getWindow('mainWindow')) {
    createWindowFocus();
  }
});

const buffer = Buffer.alloc(16 * 1024);
const fd = fs.openSync(settings.clientTxtPath, 'r');
const wr = fs.openSync('test.txt', 'w+');
if(!settings.clientTxtPath)
{
  console.error('No log file found. PeepoSad');
}else {
  fs.watchFile(settings.clientTxtPath, {interval: 10}, onFileContentChange);
}

function onFileContentChange(current: Stats, previous: Stats): void {
  fs.read(fd, buffer, 0, current.size - previous.size, previous.size, evaluateBuffer);

}

const parser = new TradeParser();

function evaluateBuffer(err: any, bytes: number, buff: Buffer): void {
  const asString: string = buff.toString('utf8', 0, bytes);
  const splitted: string[] = asString.split('\n');
  splitted.forEach(parseAndSend);

}

// TODO ONLY INCLUDED FOR TESTING DUMMY FUNCTIONALITY, REMOVE LATER!
ipcMain.on('test', (event, arg) => {
  let sizes = windowManager.mainWindow.getSize();
  console.log(sizes, sizes[0], sizes[1]);
  windowManager.mainWindow.setSize(sizes[0], sizes[1] + 100);
  console.log(screen.getPrimaryDisplay().workAreaSize);
});

// Reduce window size when trade is removed
ipcMain.on('trade-removed', (event, arg) => {
  let sizes = windowManager.mainWindow.getSize();
  console.log(sizes, sizes[0], sizes[1]);
  windowManager.mainWindow.setSize(sizes[0], sizes[1] - 100);
  console.log(screen.getPrimaryDisplay().workAreaSize);
});

function parseAndSend(splitted: string): void {
  const isTrade = parser.isTradeWhisper(splitted);
  if (isTrade) {
    const parsed = parser.parse(splitted);
    console.log(parsed);
    let sizes = windowManager.mainWindow.getSize();
    console.log(sizes, sizes[0], sizes[1]);
    windowManager.mainWindow.setSize(sizes[0], sizes[1] + 100);
    windowManager.mainWindow.webContents.send('trade-whisper', parsed);
  }

  fs.appendFile(wr, `Trade?:${isTrade} Text: \n${splitted}\n\n`, () => console.log(`appended`));
}
