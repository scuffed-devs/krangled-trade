const { BrowserWindow } = require('electron');

// Some typedefs to shorten names (I'm lazy, ok?!)
type ManagedWindow = Electron.BrowserWindow;
type WindowConfig = Electron.BrowserWindowConstructorOptions;

/**
 * The window manage may create, destroy and hold windows.
 */
export class WindowManager
{
  /**
   * Diction for windows. key = name of the window.
   * @private no direct access allowed.
   */
  private windows:  Map<string, ManagedWindow> = new Map<string, ManagedWindow>();
  private readonly mainWindowKey: string = 'mainWindow';

  /**
   * Adds a window to the diction (specified by the key).
   *
   * @param key - The key by which the window should be found.
   * @param window - The window to add.
   * @protected Not directly accessible.
   */
  protected add(key: string, window: ManagedWindow): void{
    this.windows.set(key, window);
  }

  /**
   * Removes a window from the list if it is no longer needed.
   *
   * @param key - The key of the window to destory.
   */
  public destroy(key: string)
  {
    let window = this.windows.get(key);
    // make sure the window is destroyed before being removed
    window?.destroy()
    this.windows.delete(key);
  }

  /**
   * Retrieves the main window of the application.
   * Extra access due to it's importance.
   */
  public get mainWindow(): ManagedWindow
  {
    return this.windows.get(this.mainWindowKey) as ManagedWindow;
  }

  /**
   * I don't know if I like it but for now the main window may be replaced.
   *
   * @param window - The window to set as the main window (NOTE: The main window is not the currently focused window.)
   */
  public set mainWindow( window: ManagedWindow )
  {
    this.replace(this.mainWindowKey, window)
  }

  /**
   * Remove the previous window registered by the key and replace it with the new one.
   * The old one is completely destroyed before being replaced.
   *
   * @param key - The key to identify the window by.
   * @param window - The window to set instead of the old one.
   */
  public replace(key: string, window: ManagedWindow)
  {
    // Destroy the old one
    this.destroy(key);
    // and simply add the new one.
    this.add(key, window);
  }

  /**
   * Retrieves the window for the key.
   *
   * @param key- The key to look for.
   */
  public getWindow( key: string ): ManagedWindow
  {
    return this.windows.get(key) as ManagedWindow;
  }

  /**
   * Creates a window for the given key and the given options. The window will be added immediately to this window manager right after instantiation.
   *
   * @param key - The key to add the window for.
   * @param options - The options for window creation.
   * @see WindowManager#add
   */
  public createManagedWindow(key: string, options?: WindowConfig): ManagedWindow
  {
    if(key === this.mainWindowKey)
    {
      throw `Key may not be [${this.mainWindowKey}]`;
    }
    let window: ManagedWindow = this.createWindow(options);
    this.add(key, window);
    return window;
  }

  /**
   * Creates an unmanaged window. Plain and simple. Does nothing else.
   *
   * @param options - The options for window creation
   * @param callback - Callback to handle the newly created window.
   */
  public createWindow(options?: WindowConfig, callback?: (window: ManagedWindow) => void): Electron.BrowserWindow
  {
    let window = new BrowserWindow(options)
    if(callback)
    {
      callback(window)
    }
    return window;
  }
}
