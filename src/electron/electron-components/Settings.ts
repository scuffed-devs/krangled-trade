import path = require('path');
import Electron = require('electron');
const fs = require('fs');

export class Settings
{
  private static userDataPath: string = Electron.app.getPath('userData');
  private readonly _fileName: string;
  public posX: number = -1;
  public posY: number = -1;
  public clientTxtPath: string | undefined;

  public static from(fileName: string): Settings
  {
    return new Settings(fileName);
  }

  private constructor(fileName: string) {
    this._fileName = fileName;
  }

  // For now has to be called manually after update.
  public writeSettings():void
  {
    console.log('property changed and gets written');
    let json = JSON.stringify(this);
    Object.keys(this).filter(key => key[0] === "_").forEach(key => { json = json.replace(key, key.substring(1)); });
    let joined = path.join(Settings.userDataPath, `${this._fileName}.json`);
    fs.writeFileSync(joined, json, (err: any) => {if(err) console.error(`Error: %${err}`)});
  }

  static readSettings(fileName: string): Settings
  {
    let joined = path.join(Settings.userDataPath, `${fileName}.json`)
    let json: string = fs.readFileSync(joined);
    let dataCore = JSON.parse(json);
    let settings: any = new Settings(fileName) as unknown;
    Object.keys(dataCore).filter(key => key !== 'fileName').forEach( key => settings[key] = dataCore[key]);
    return settings;
  }
}
