import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeWhisperItemComponent } from './trade-whisper-item.component';

describe('TradeWhisperItemComponent', () => {
  let component: TradeWhisperItemComponent;
  let fixture: ComponentFixture<TradeWhisperItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeWhisperItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeWhisperItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
