import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {TradeWhisper} from '../../../classes/TradeWhisper';

@Component({
  selector: 'app-trade-whisper-item',
  templateUrl: './trade-whisper-item.component.html',
  styleUrls: ['./trade-whisper-item.component.css']
})
export class TradeWhisperItemComponent implements OnInit {

  @Input() item: TradeWhisper | undefined;
  @Output() deleteWhisper: EventEmitter<TradeWhisper> = new EventEmitter<TradeWhisper>();
  constructor() { }

  ngOnInit(): void {
  }

  onDelete(item: TradeWhisper | undefined): void{
  this.deleteWhisper.emit(item);
  }

}
