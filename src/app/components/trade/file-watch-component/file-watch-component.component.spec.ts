import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileWatchComponentComponent } from './file-watch-component.component';

describe('FileWatchComponentComponent', () => {
  let component: FileWatchComponentComponent;
  let fixture: ComponentFixture<FileWatchComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileWatchComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileWatchComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
