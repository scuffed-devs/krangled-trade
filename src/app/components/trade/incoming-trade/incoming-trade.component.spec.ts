import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingTradeComponent } from './incoming-trade.component';

describe('IncomingTradeComponent', () => {
  let component: IncomingTradeComponent;
  let fixture: ComponentFixture<IncomingTradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncomingTradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
