import {Component, NgZone, Input} from '@angular/core';
import {TradeWhisper} from './classes/TradeWhisper';
import { IpcService } from './services/ipc.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'krangled-trade';

  num = 1;

  @Input() whispers: TradeWhisper[] = [];
  ipc: IpcService | undefined;
  zone: NgZone;


  constructor(zone: NgZone, ipc: IpcService) {
    this.zone = zone;
    this.ipc = ipc;

    this.ipc?.on('trade-whisper', (event: any, args: any) => this.addWhisper(args));
    this.ipc?.on('test-reply', (event, args) => console.log(args));
  }

  onClick(): void{
    this.ipc?.send('test', 'ping');
    const dummy: TradeWhisper = new TradeWhisper();
    dummy.stashPosX = 4;
    dummy.stashPosY = 2;
    dummy.uuid = this.num.toString();
    this.num++;
    this.whispers.push(dummy);
  }

  private addWhisper(args: TradeWhisper): void {
    console.log(`Received_ PosX: ${args.stashPosX} | PosY ${args.stashPosY}`);
    this.zone.run(() => { this.whispers.push(args); } );
  }

  onDelete(item: TradeWhisper): void{
    this.whispers = this.whispers.filter( whisp => whisp.uuid !== item.uuid);
    this.ipc?.send('trade-removed', item);
    if (!this.whispers.length)
    {
      this.ipc?.send('empty-whispers', 'Its empty y\'all');
    }
  }
}
