import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { IncomingTradeComponent } from './components/trade/incoming-trade/incoming-trade.component';
import { FileWatchComponentComponent } from './components/trade/file-watch-component/file-watch-component.component';
import { TradeWhisperItemComponent } from './components/trade/trade-whisper-item/trade-whisper-item.component';

@NgModule({
  declarations: [
    AppComponent,
    IncomingTradeComponent,
    FileWatchComponentComponent,
    TradeWhisperItemComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
