import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';

@Injectable({
  providedIn: 'root'
})
/**
 * This service is wrapping
 */
export class IpcService {

  private ipc: IpcRenderer;

  constructor()
  {
    this.ipc = window.require('electron').ipcRenderer;
  }

  public on(token: string, listener: (event: any, args: any) => void): void{
    this.ipc.on(token, listener);
  }

  public send(token: string, payload: any): void
  {
    this.ipc.send(token, payload);
  }

}
